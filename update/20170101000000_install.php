<?
class wp_toplist__20170101000000_install
{
	function __construct(){ global $C, $D; $this->C = &$C; $this->D = &$D; }
	function __call($m, $a){ return $a[0]; } 
	function up()
	{
		$this->C->db()->query("CREATE TABLE `toplist_list` (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(32) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `text` varchar(200) DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  `utimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `itimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf32_unicode_ci");

		$this->C->db()->query("CREATE TABLE `toplist_statistics` (
  `id` date NOT NULL,
  `list_id` varchar(32) NOT NULL,
  `user_id` varchar(32) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `visit` int(10) UNSIGNED DEFAULT '0',
  `in` int(10) UNSIGNED DEFAULT '0',
  `out` int(10) UNSIGNED DEFAULT '0',
  `utimestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `itimestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf32_unicode_ci;");

		$this->C->db()->query("ALTER TABLE `toplist_list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);");
		$this->C->db()->query("ALTER TABLE `toplist_statistics`
  ADD PRIMARY KEY (`id`,`list_id`) USING BTREE,
  ADD KEY `list_id` (`list_id`),
  ADD KEY `user_id` (`user_id`);");
		return 1;
	}
	
	function down()
	{
		return 1;
	}
}