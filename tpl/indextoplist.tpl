{include file="system/bootstrap/tpl/header.tpl"}
<table class="table">
	<thead>
		<tr>
			<th>Nr</th>
			<th style="text-align:center;">Banner</th>
			<th>visit</th>
			<th>In</th>
			<th>Out</th>
		</tr>
	</thed>
	<tbody>

	{foreach from=$D.TOPLIST.D name=TL key=kTL item=TL}
		<tr>
			<td style="width:50px;">{$smarty.foreach.TL.iteration}</td>
			<td style="text-align:center;">
				<div>{$TL.TITLE}</div>
				<div><img alt="{$TL.TITLE}" src="data/toplist_{$kTL}.486x60.jpg"></div>
				<div>{$TL.TEXT}</div>
			</td>
			<td style="width:50px;">{$TL.YEAR.D[{$smarty.now|date_format:"%Y"}].MONTH.D[{($smarty.now|date_format:"%m")*1}].VISIT}<br>({$TL.VISIT})</td>
			<td style="width:50px;">{$TL.YEAR.D[{$smarty.now|date_format:"%Y"}].MONTH.D[{($smarty.now|date_format:"%m")*1}].IN}<br>({$TL.IN})</td>
			<td style="width:50px;">{$TL.YEAR.D[{$smarty.now|date_format:"%Y"}].MONTH.D[{($smarty.now|date_format:"%m")*1}].OUT}<br>({$TL.OUT})</td>
		</tr>
	{/foreach}
	</tbody>
</table>
{include file="system/bootstrap/tpl/footer.tpl"}