<?
class toplist__class__toplist extends toplist__class__toplist__parent
{
	function get_list()
	{
		$W = ($this->D['TOPLIST']['W'])? " AND {$this->D['TOPLIST']['W']}":'';

		$qry = $this->C->db()->query("SELECT id,user_id,active,title,text,url,utimestamp,itimestamp FROM toplist_list WHERE 1 {$W} ORDER BY (SELECT SUM(visit) FROM toplist_statistics WHERE list_id = toplist_list.id) DESC");
		while ($ary = $qry->fetch_array(MYSQLI_ASSOC))
		{
			$this->D['TOPLIST']['D'][ $ary['id'] ] = array(
				'USER_ID'		=> $ary['user_id'],
				'ACTIVE'		=> $ary['active'],
				'TITLE'			=> $ary['title'],
				'TEXT'			=> $ary['text'],
				'URL'			=> $ary['url'],
				'ITIMESTAMP'	=> $ary['itimestamp'],
				'UTIMESTAMP'	=> $ary['utimestamp'],
			);
		}
		
		$qry = $this->C->db()->query("SELECT id,list_id,user_id,active,SUM(`in`) AS `in`,SUM(visit) visit, SUM(`out`) `out`, YEAR(id) as y, MONTH(id) as m, DAY(id) as d, utimestamp,itimestamp FROM toplist_statistics WHERE 1 {$W} GROUP BY id");
		while ($ary = $qry->fetch_array(MYSQLI_ASSOC))
		{
			$this->D['TOPLIST']['D'][ $ary['list_id'] ]['YEAR']['D'][ $ary['y'] ]['IN']		+= $ary['in'];
			$this->D['TOPLIST']['D'][ $ary['list_id'] ]['YEAR']['D'][ $ary['y'] ]['OUT']	+= $ary['out'];
			$this->D['TOPLIST']['D'][ $ary['list_id'] ]['YEAR']['D'][ $ary['y'] ]['VISIT']	+= $ary['visit'];
			$this->D['TOPLIST']['D'][ $ary['list_id'] ]['YEAR']['D'][ $ary['y'] ]['MONTH']['D'][ $ary['m'] ]['IN']		+= $ary['in'];
			$this->D['TOPLIST']['D'][ $ary['list_id'] ]['YEAR']['D'][ $ary['y'] ]['MONTH']['D'][ $ary['m'] ]['OUT']		+= $ary['out'];
			$this->D['TOPLIST']['D'][ $ary['list_id'] ]['YEAR']['D'][ $ary['y'] ]['MONTH']['D'][ $ary['m'] ]['VISIT']	+= $ary['visit'];
			$this->D['TOPLIST']['D'][ $ary['list_id'] ]['YEAR']['D'][ $ary['y'] ]['MONTH']['D'][ $ary['m'] ]['VISIT'].'|';
			$this->D['TOPLIST']['D'][ $ary['list_id'] ]['YEAR']['D'][ $ary['y'] ]['MONTH']['D'][ $ary['m'] ]['DAY']['D'][ $ary['d'] ]['IN']		+= $ary['in'];
			$this->D['TOPLIST']['D'][ $ary['list_id'] ]['YEAR']['D'][ $ary['y'] ]['MONTH']['D'][ $ary['m'] ]['DAY']['D'][ $ary['d'] ]['OUT']	+= $ary['out'];
			$this->D['TOPLIST']['D'][ $ary['list_id'] ]['YEAR']['D'][ $ary['y'] ]['MONTH']['D'][ $ary['m'] ]['DAY']['D'][ $ary['d'] ]['VISIT']	+= $ary['visit'];
			$this->D['TOPLIST']['D'][ $ary['list_id'] ]['IN']		+= $ary['in'];
			$this->D['TOPLIST']['D'][ $ary['list_id'] ]['OUT']		+= $ary['out'];
			$this->D['TOPLIST']['D'][ $ary['list_id'] ]['VISIT']	+= $ary['visit'];
		}
	}
	
	function set_list()
	{
		foreach($this->D['TOPLIST']['D'] as $k => $v )
		{
			if($v['ACTIVE'] != -1)
			{
				$IU .= (($IU)?',':'')."('{$k}','{$v['USER_ID']}','{$v['ACTIVE']}'";
				$IU .= (isset($v['TITLE']))?", '{$v['TITLE']}'":", NULL";
				$IU .= (isset($v['TEXT']))?", '{$v['TEXT']}'":", NULL";
				$IU .= (isset($v['URL']))?", '{$v['URL']}'":", NULL";
				$IU .= ")";
			}
			else
			{
				$DEL .= ($DEL?',':'')."'{$k}'";
			}
		}
		
		if($IU)#ToDo: Insert Update; chanel_id
			$this->C->db()->query("INSERT INTO toplist_list (id,user_id,active,title,text,url) VALUES {$IU}
									ON DUPLICATE KEY UPDATE
									user_id = CASE WHEN VALUES(user_id) IS NOT NULL THEN VALUES(user_id) ELSE toplist_list.user_id END,
									active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE toplist_list.active END,
									title = CASE WHEN VALUES(title) IS NOT NULL THEN VALUES(title) ELSE toplist_list.title END,
									text = CASE WHEN VALUES(text) IS NOT NULL THEN VALUES(text) ELSE toplist_list.text END,
									url = CASE WHEN VALUES(url) IS NOT NULL THEN VALUES(url) ELSE toplist_list.url END
									");
		if($DEL)
			$this->C->db()->query("DELETE FROM toplist_list WHERE id IN ({$DEL})");
	}
}